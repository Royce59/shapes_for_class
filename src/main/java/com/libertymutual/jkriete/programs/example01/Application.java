package com.libertymutual.jkriete.programs.example01;

import java.awt.Color;
import java.math.BigDecimal;
import com.libertymutual.jkriete.programs.example01.shapes.Shape;
import com.libertymutual.jkriete.programs.example01.shapes.Circle;
import com.libertymutual.jkriete.programs.example01.shapes.Square;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

    private final static Logger logger = LogManager.getLogger(Application.class);
     
    public static void main(String[] args) {

        logger.info(Application.class + " starting ");
        
        int radius = 10;
        Circle circle = new Circle(radius, Color.PINK);
        BigDecimal area = circle.getArea();
        System.out.println(area);

        int length = 100;
        Square square = new Square(length, Color.RED);
        System.out.println(square.getArea());
    }
}
