package com.libertymutual.jkriete.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;

// public BigDecimal getArea();
// public Color getColor();

abstract public class Shape {

    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public abstract BigDecimal getArea();

    public Color getColor() {
        return color;
    }
}
