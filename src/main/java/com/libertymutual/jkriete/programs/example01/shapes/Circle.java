package com.libertymutual.jkriete.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class Circle extends Shape {

    private final static Logger logger = LogManager.getLogger(Circle.class);

    private int radius;

    public Circle(int radius, Color color) {
        super(color);
        this.radius = radius;
    }

    // provide a getArea implementation 
    @Override
    public BigDecimal getArea() {

		logger.info(Circle.class + " starting ");
        double pi = 3.14;
        double area = pi * radius * radius;
        return new BigDecimal(area);

    }
}
